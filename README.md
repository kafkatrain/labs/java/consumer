# Consumer Kafka Exemplo

Consumer Kafka simples.

## Requerimentos

- Java (JDK) 1.8
- Kafka

## Executar

Escolha um dos comandos abaixo para executar. Se for a primeira vez que
você faz isso, levará um tempo até que todas as dependências estejam 
disponíveis.

- `cfg.file`: definir o caminho para um arquivo com configurações do consumer. 
- `cfg`: é um argumento para configurações adicionais do consumidor. Valores definidos
aqui tem prioridade sobre àqueles definidos por `cfg.file`

__Linux__

Exemplo com `cfg.file`: 

```bash
source /home/fabiojose/kafka/my-single-tls/artifact/config-oauth-consumer.env

./gradlew run \
  -Dkafka="$(cat /home/fabiojose/kafka/my-single-tls/artifact/my-single-oauth.host.port.oauth)" \
  -Dgrupo=kafkatrain \
  -Dtopico='my-topic' \
  -Dpoll='5000' \
  -Dcfg.file=/home/fabiojose/kafka/my-single-tls/artifact/config-oauth.properties \
  -Djavax.net.ssl.trustStore=/home/fabiojose/kafka/my-single-tls/artifact/my-single-oauth.truststore.p12 \
  -Djavax.net.ssl.trustStorePassword=changeit \
  -Djavax.net.ssl.trustStoreType=PKCS12
```

Exemplo com `cfg`:

```bash
./gradlew run \
  -Dkafka=localhost:9092 \
  -Dgrupo=kafkatrain \
  -Dtopico='meu-topico' \
  -Dpoll='5000' \
  -Dcfg='fetch.min.bytes=256, auto.offset.reset=latest'
```

__Windows__

```powershell
.\gradlew.bat run ^
-Dkafka=localhost:9092 ^
-Dgrupo=kafkatrain ^
-Dtopico='<NOME DO TOCIPO>' ^
-Dpoll='5000' ^
-Dcfg='cfg1=v1, cfg2=v2'
```

Exemplo:
```powershell
.\gradlew.bat run ^
-Dkafka=localhost:9092 ^
-Dgrupo=kafkatrain ^
-Dtopico='meu-topico' ^
-Dpoll='5000' ^
-Dcfg='fetch.min.bytes=256, auto.offset.reset=latest'
```